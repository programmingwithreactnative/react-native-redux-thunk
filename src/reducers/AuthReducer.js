import {EMAIL_CHANGED, PASSWD_CHANGED,LOGIN_USER_SUCCESS,LOGIN_USER_FAIL,LOGIN_USER} from '../actions/Types';

/**
 * setting the initials states for the pieces that this reducer is reponsible for is 
 * kind of a bext practice to increase readability
 */
const INITIAL_STATE = {
    email: 'serdar-o@hotmail.com', 
    password:'serdar',
    error: '',
    user: null,
    loading: false
};
export default (state=INITIAL_STATE, action) => {
    console.log(action);
    switch (action.type){
        case EMAIL_CHANGED:
            /**
             * if we want to modify our state object we cannot just update our existing
             * state object. we need to return a new state object otherwise redux will
             * see that the object (reference to state object) is the same so it has not changed.
             * this is very important! whenever a piece of state is updated, a new state object should
             * be returned here!
             *   state.email = action.payload;
             *   return state;
             * above won't work!
             * below is the right way to do this. it means, take the existing state's properties, 
             * put them in a new object, override the 'email' prop value, return this new object.
             */
            return {...state, email: action.payload};
        case PASSWD_CHANGED:
            return {...state, password: action.payload};
            /** 
             * clearing the email and password so that they wont be filled if user comes back to login screen
            */
        case LOGIN_USER_SUCCESS:
            return {...state, 
                user: action.payload, 
                error: '', 
                loading:false,
                email: '',
                password:''
            };
            /** 
             * above case can also be implemented like so to clear/reset all the state pieces:
             * {...state, ...INITIAL_STATE, user:action.payload}
            */
        case LOGIN_USER_FAIL:
            return {...state, error: 'Authentication Failed.', loading:false};
        case LOGIN_USER:
            return {...state, loading: true, error:''};
        default:
            return state;
    }
};