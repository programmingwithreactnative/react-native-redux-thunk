import {EMPLOYEES_FETCH_SUCCESS} from '../actions/Types';

/** 
 * our initial state is an object because eventually we will be getting an object from firebase
 * containing all the employees
*/
const INITIAL_STATE = {};

export default (state=INITIAL_STATE, action) => {
    switch (action.type){
        case EMPLOYEES_FETCH_SUCCESS:
            //console.log(action);
            return action.payload;
        default:
            return state;
    }

};