import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
//all touchable variants are used to create buttons with different feedbacks
//just wrap other components with touchable variants to make them behave like buttons
//for better reusability we are passing "onPresss" (an arbitrary name) from parent which is AlbumDetail to the
//onPress property of the TouchableOpacity component
//observe the use of {children} below and how we pass it from the calling AlbumDetails to avoid
//hard coding the button text and make it more reusable
const Button = ({ onPresss, children }) => {
  const { buttonStyle, textStyle } = styles;
  return (
    <TouchableOpacity onPress={onPresss} style={buttonStyle}>
      <Text style={textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#007aff',
    marginLeft: 5,
    marginRight: 5
  },
  textStyle: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  }
};
export  {Button};
