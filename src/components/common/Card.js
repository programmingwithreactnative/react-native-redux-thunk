import React, { Component } from 'react';
import { View } from 'react-native';

//in react native since we dont have the benefit of seperate css files it is common
//to make standalone components whole sole purpose is to add some styling to the app
//anytime we pass, to a component that we write, another component that component will showup
//in the  props object as "props.children"
//Card will render 'any components' that we pass to it using props.children (like in AlbumDetail.js)
const Card = (props) => {
  return (
    <View style={styles.containerStyle}>
      {props.children}
    </View>
  );
};

const styles = {
  //remember. these names like containerStyle below are arbitrary.
  //use whatever makes sense to you
  containerStyle: {
      borderWidth: 1,
      borderRadius: 2,
      borderColor: '#ddd',
      borderBottomWidth: 0,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 }, //no shadow on the left or right just on the bottom
      shadowOpacity: 0.2,
      shadowRadius: 2, // match borderRadius here for a consistent look
      elevation: 1,
      marginLeft: 5,
      marginRight: 5,
      marginTop: 10
  }
};

export  {Card};
