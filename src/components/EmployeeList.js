import _ from 'lodash';
import ListItem from './ListItem';
import {View, Text} from 'react-native';
import React, {Component} from 'react';
import {ListView, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {employeesFetch} from '../actions';

class EmployeeList extends Component{
    componentDidMount(){
        this.props.employeesFetch();
        /** 
         * THIS MECHANISM IS NOT USED HERE - IGNORE BELOW COMMENTS - KEEPING FOR REFERENCE PURPOSES
         * 
         * by the time below line of code executes, this.props.employees will probably be empty since we wil be
         * waiting for the async firebase request
         * any time any piece of state changes the connect helper at the bottom will re-run mapStateToProps. even yhough our component 
         * may not immediately get data, eventually it will.
         * 
         * we can use componentWillReceiveProps lifecycle method to get a handle on that received new data. this method runs whenever 
         * we are about to receive a new set of props to re-render the component with. it get called with new set of props that the
         * component gets. conventionally this argument is called nextProps. this.props will still include the old set of props at this point.
         * componentWillReceiveProps will have access to both sets. 
         * 
         * when user naigates to EmployeeCreate, this EmployeeList component will be trashed. when they come bask to this page, a new 
         * instance EmployeeList component is created, componentWillMount method will be triggered again. at this point we already have all 
         * the employees in our global state. it would be beneficial to build our datasource both when it first loads up and when it receives
         * new props.
        */
        //this.createDataSource(this.props);
    }
    // componentWillReceiveProps(nextProps){
    //     this.createDataSource(nextProps);
    // }
    // createDataSource({employees}){
    //     const ds = new FlatList.DataSource({
    //         rowHasChanged: (r1, r2) => r1 !== r2 
    //     });
    //     this.dataSource = ds.cloneWithRows(employees);
    // }
    // renderRow(employee){
    //     console.log("renderRow");
    //     return <ListItem employee={employee} />;
    // }
    render(){
        //console.log(this.props);
        return(
            <FlatList 
                data={this.props.employees}
                renderItem={
                    employee => <ListItem employee={employee} />  
                }
                keyExtractor={employee=>employee.uid}
            />
        );
    }
}
/** 
 * npm install --save lodash -> "--SAVE" OPTIONS IS NOT REQUIRED SINCE NPM 5.0.0
 * lodash is a helper library for working with objects and arrays.
 * we will use it to convert our object (collection of employees returned from firebase as an object)
 * to an array of employees.
 * 
 * explanation:
 * state.employees is an object, it has many key-value pairs.
 * for each pair, run this fat arrow function(defined inside _.map). fat arror function is called with each value and key.
 * key is the id of the record. val is the user model. it has name, shift, phone properties.
 * 
 * we then create a new object, we push all the values from the user modeul (...val) and we throw the id on top
 * return {...val, uid}. uid is the unique id of a record.
 * we craft an object like this -> {shift: 'monday', name:'serdar', phone:'555', id:'1231235'} for each 
 * object in the array. then we called all those objects and put them into an array (employees). this is what 
 * our DataSource object expects!
 * 
 * state.employees is a piece of state created by our reducer. employees on the left is the array we are creating
 * and returning to our component to be rendered!
 * 
 * puhsing the returned objects into employees array is handled by map method for us.
*/
const mapStateToProps = state => {
    const employees = _.map(state.employees, (val, uid)=>{
        return {...val, uid};
    }); 
    /** 
     * it is like {employees:employees} this is an ES6 shortcut. here we are returning an object with key employees,
     * value employees (an array). if we write return employees we would be returning an array anc connect would not
     * accept this. 
     * uncomment the below console.log statements to see what is returned as the payload and what its tranformed
     * version into an array.
     * 
     * return as {employees} to be able to acces the data using "employees" key. Also, mapStateTopProps must
     * return an object. so we return as {employees}.
    */
    //console.log(state.employees);
    //console.log({employees});
    //console.log(employees);
    return {employees};//we will access this using this.props.employees inside our component
};

export default connect(mapStateToProps,{employeesFetch})(EmployeeList);