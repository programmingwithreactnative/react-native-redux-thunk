import React, {Component} from 'react';
import {Card, CardSection, Button} from './common';
import {connect} from 'react-redux';
import {employeeUpdate,employeeCreate} from '../actions';
import EmployeeForm from './EmployeeForm';

/** 
 * remember that "text" naming inside onChangeText method call is arbitrary.
*/
class EmployeeCreate extends Component{
    onButtonPress(){
        const {name, phone, shift} = this.props;
        this.props.employeeCreate({name,phone,shift:shift || 'Monday'});
    }
    render(){
        /** 
         * take all the props of EmployeeCreate and forward them to EmployeeForm
         * {...this.props}
        */
        return(
            <Card>
                <EmployeeForm {...this.props}/>
                <CardSection>
                    <Button onPresss={this.onButtonPress.bind(this)}>
                        Create
                    </Button>
                </CardSection>
            </Card>
        );
    }
}

const mapStateToProps = (state)=>{
    const {name, phone, shift} = state.employeeForm;
    return {name, phone, shift};
}
export default connect(mapStateToProps,{employeeUpdate, employeeCreate})(EmployeeCreate);