import React, {Component} from 'react';
import {Card, CardSection, Button, Confirm} from './common';
import EmployeeForm from './EmployeeForm';
import {connect} from 'react-redux';
import {employeeUpdate,employeeSave, employeeDelete} from '../actions';
import _ from 'lodash';
import Communications from 'react-native-communications';

class EmployeeEdit extends Component{
    state = {showModal:false};
    /** 
     * iterate over every property of employee object and update our reducer with those properties (form reducer)
     * we could also create a new action creator and update all the values in our reducer 
    */
    componentWillMount(){
        _.each(this.props.employee.item,(value,prop)=>{
            this.props.employeeUpdate({prop,value});
        });
    }
    onButtonPress(){
        const {name, phone, shift} = this.props;
        console.log(name,phone,shift);
        /** 
         * this.props.employee comes from ListItem's onRowPress
        */
        this.props.employeeSave({name,phone,shift,uid:this.props.employee.item.uid});
    }
    onTextPress(){
        const {phone, shift} = this.props;
        Communications.text(phone, `Your upcoming shift is on ${shift}`);
    }
    onAccept(){
        const {uid} = this.props.employee.item;
        this.props.employeeDelete({uid});
    }
    onDecline(){
        this.setState({showModal:false});
    }
    render(){
        return(
            <Card>
                <EmployeeForm />
                <CardSection>
                    <Button onPresss={this.onButtonPress.bind(this)}>
                        Save Changes
                    </Button>
                </CardSection>
                <CardSection>
                    <Button onPresss={this.onTextPress.bind(this)}>
                        Text Schedule
                    </Button>
                </CardSection>
                <CardSection>
                    <Button onPresss={()=>this.setState({showModal:!this.state.showModal})}>
                        DELETE
                    </Button>
                </CardSection>
                <Confirm
                visible={this.state.showModal}
                onAccept={this.onAccept.bind(this)}
                onDecline={this.onDecline.bind(this)}
                >
                    Are you sure you want to delete?
                </Confirm>
            </Card>
        );
    }
}
const mapStateToProps = state =>{
    const {name, phone, shift} = state.employeeForm;
    return {name,phone,shift};
};
export default connect(mapStateToProps, {employeeUpdate,employeeSave, employeeDelete})(EmployeeEdit);