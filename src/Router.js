/** 
 * npm install --save react-native-router-flux
 * we can have a single parent scene inside Router. all other scenes should be defined as children
 * of this scene.
*/
import React from 'react';
import {Scene, Router, Actions} from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';
import EmployeeEdit from './components/EmployeeEdit';

/** 
 * Here we wrap scenes with some parent scenes if we dont auto-generated want back buttons
 * between them. when we do Actions.main in index.js for actions, main will show the first
 * child scene it surounds.
*/
const RouterComponent = () => {
    return(
        <Router>
            <Scene key="root" hideNavBar>
                <Scene key="auth">
                    <Scene key="login" component={LoginForm} title="Login" titleStyle={styles.titleStyle} initial />
                </Scene>
                <Scene key="main">
                    <Scene 
                        rightTitle="Add"
                        onRight={()=>Actions.employeeCreate()}
                        key="employeeList" 
                        component={EmployeeList} 
                        titleStyle={styles.titleStyle} 
                        title="Employee List" 
                        initial
                    />
                    <Scene
                        key="employeeCreate"
                        component={EmployeeCreate}
                        title="Create Employee"
                    />
                    <Scene
                        key="employeeEdit"
                        component={EmployeeEdit}
                        title="Edit Employee"
                    />
                </Scene>   
            </Scene>
        </Router>
    );
};
const styles = {
    titleStyle: {
        flex: 1,
        textAlign: 'center'
    }
}
export default RouterComponent;