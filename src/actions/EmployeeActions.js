import { EMPLOYEE_UPDATE, EMPLOYEE_CREATE, EMPLOYEES_FETCH_SUCCESS,EMPLOYEE_SAVE_SUCCESS } from './Types';
import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

export const employeeUpdate = ({prop, value}) => {
    return {
        type: EMPLOYEE_UPDATE,
        payload: {prop, value} //ES6 notation {prop:prop, value:value}
    };
};

export const employeeCreate = ({name,phone,shift})=>{
    console.log(name, phone,shift);
    const {currentUser} = firebase.auth(); //firebase.auth().currentUser -> curently authenticated user model
/** 
 * here we are exploiting redux thunk here.
 * we are not actually dispatching anything here. we are just getting past the error saying:
 * "actions must be plain objects. use custom middleware for async actions".
 * 
 * below we use Actions.pop to go back to the previous screen. we can also use below to go any other screen, 
 * and we use "type: reset" in order to prevent back button to the current screen. dont thread target view like a 
 * view we will advance to. reset the entire view stack:
 * 
 * Actions.employeeList({type:'reset'});
*/
    return (dispatch)=>{
        firebase.database().ref(`users/${currentUser.uid}/employees`)
        .push({name, phone, shift})
        .then(()=>{
            dispatch({type: EMPLOYEE_CREATE});
            Actions.pop();
        });
    };
};

export const employeesFetch = () =>{
    /** 
     * since this result will be fetched asynchronously we will use redux thunk here.
     * we will return a fat arrow functions that gets called with the dispatch method.
     * 
     * snapshot is an object that describes the data. we get the data using val() method.
     * 
     * .on('value', snapshot => ...) means below:
     * any time any data comes from this ref, call this fat arrow function with an object 
     * to describe the data (snapshot) that is sitting in there.
     * 
     * .on('value') -> this method is persistent. for the life of our application the dispatch function inside 
     * will be called anytime a new value is came across. 
    */
   const {currentUser} = firebase.auth();
   return (dispatch) => {
    firebase.database().ref(`users/${currentUser.uid}/employees`)
    .on('value', snapshot => {
        dispatch({type:EMPLOYEES_FETCH_SUCCESS, payload: snapshot.val()});
    });
   };
};

export const employeeSave = ({name, phone, shift, uid}) => {
    const {currentUser} = firebase.auth();
    /** 
     * return a function. use redux-thunk. this will be an asynch action creator.
     * we wont be returning anything right away from this action creator.
     * 
     * EmployeeFrom state is being used both in EmployeeEdit and EmployeeCreate forms. we need to reset the properties on FormReducer
     * after editing an employee, otherwise when we hid add botton, we will get a pre-filled form with the latest edit values.
     * as soon as we save the changes and we click the edited user on the list to edit again, we see the updated values.
     * these updated values are fetched by the constantly monitoring on('value',snapshot=>.... method above inside employeesFetch
     * action creator. remember. it is running through the lifecycle of our application and fetching updated data as data changes
     * on firebase.
    */
    return (dispatch) => {
        firebase.database().ref(`users/${currentUser.uid}/employees/${uid}`)
        .set({name, phone, shift})
        .then(()=>{
            dispatch({type:EMPLOYEE_SAVE_SUCCESS});
            Actions.employeeList({type:'reset'});
        });
    };
};
export const employeeDelete = ({uid})=>{
    const {currentUser} = firebase.auth();
    /** 
     * we dont need dispatch here. we dont need another action creator. we already get updated data.
     * see above employeesFetch action creator.
    */
    return () =>{
        firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
        .remove()
        .then(()=>{
            Actions.employeeList({type:'reset'});
        });
    };
};