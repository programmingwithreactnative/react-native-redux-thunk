import firebase from 'firebase';
import {EMAIL_CHANGED, PASSWD_CHANGED,LOGIN_USER_SUCCESS, LOGIN_USER_FAIL, LOGIN_USER} from './Types';
import {Actions} from 'react-native-router-flux';

export const emailChanged = (text) => {
    return{
        type: EMAIL_CHANGED,
        payload:text
    };
};

export const passwordChanged = (text) => {
    return{
        type: PASSWD_CHANGED,
        payload:text
    };
};
/**
 * we use ReduxThunk to create asynchronous action creators!
 * it can be used for any type of asynchronous long running process, not just for firebase.
 * ReduxThunk is an npm module we need to install and setup.
 * with redux thunk the action creators can now return functions not just objects with type property.
 * returned function is dispatched to reducers with "dispatch". we do the dispatching manually in this case,
 * as can be seen below.
 * we wire ReduxThunk in the App.js using createStore method using applyMiddleware.
 */
export const loginUser = ({email, password}) => {
    return (dispatch) => {

        dispatch({type: LOGIN_USER});//dispatched for showing progress

        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(user=>loginUserSuccess(dispatch, user))
            //only after this asynch action is complete we then manually dispatch an action.
            //since we have direct access to dispatch, we can dispatch as many actions as we want
        .catch(()=>{
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(user=>loginUserSuccess(dispatch, user))
            .catch(()=> loginUserFail(dispatch));
        });
    };
};
const loginUserSuccess = (dispatch, user)=>{
    dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: user
    });
    /** 
     * main name comes from the "key" of the related scene in react-native-router-flux.
    */
   Actions.main();
};
const loginUserFail = (dispatch) => {
    dispatch({
        type: LOGIN_USER_FAIL
    });
};