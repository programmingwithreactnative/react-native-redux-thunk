import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducers from './reducers';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import LoginForm from './components/LoginForm';
import Router from './Router';

/** 
 * redux-thunk us a middleware. to apply any middleware to redux we need to also import 
 * applyMiddleware from redux.
*/
class App extends Component{
    componentWillMount(){
        const config = {
            apiKey: 'AIzaSyDZxExOBdRpaZ75AmlUNsRutw8V_NHbDvI',
            authDomain: 'manager-d87fb.firebaseapp.com',
            databaseURL: 'https://manager-d87fb.firebaseio.com',
            projectId: 'manager-d87fb',
            storageBucket: 'manager-d87fb.appspot.com',
            messagingSenderId: '507265647704'
        };
        firebase.initializeApp(config);
    }
    render(){
        /** 
         * second argument is optional. if we want some initial state to pass to our redux application.
         * 3rd argument is what is called store enhancer. because it adds additional functionality to store.
        */
        const store = createStore(reducers,{},applyMiddleware(ReduxThunk));
        return(
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}
export default App;